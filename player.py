#Adventure Tutorial by Eric Wideman 07/25/2016

import items
import world

class Player:
	def __init__(self):
		self.inventory = [items.Rock(), items.Dagger(), "Gold(5)", items.CrustyBread(), items.CrustyBread(), items.CrustyBread()]
		self.x = 2
		self.y = 3 
		self.hp = 100
		
	def print_inventory(self):
		print("Inventory: ")
		for item in self.inventory:
			print("* " + str(item))
		best_weapon = self.most_powerful_weapon()
		print("Your best weapon is your {}".format(best_weapon))
		print("")
		
	def most_powerful_weapon(self):
		max_damage = 0
		best_weapon = None
		for item in self.inventory:
			try:
				if item.damage > max_damage:
					best_weapon = item
					max_damage = item.damage
			except AttributeError:
				pass
				
		return best_weapon
		
	def move(self, dx, dy):
		self.x += dx
		self.y += dy
		
	def move_north(self):
		self.move(dx = 0, dy = -1)
		print("You went north.")
		print("")
		
	def move_south(self):
		self.move(dx = 0, dy = 1)
		print("You went south.")
		print("")
		
	def move_east(self):
		self.move(dx = 1, dy = 0)
		print("You went east.")
		print("")
		
	def move_west(self):
		self.move(dx = -1, dy = 0)
		print("You went west.")
		print("")
		
	def attack(self):
		best_weapon = self.most_powerful_weapon()
		room =  world.tile_at(self.x, self.y)
		enemy = room.enemy
		print("You use your {} against the {}!".format(best_weapon.name, enemy.name))
		enemy.hp -= best_weapon.damage
		if not enemy.is_alive():
			print("You killed the {}!".format(enemy.name))
			print("")
		else:
			print("{} HP is {}.".format(enemy.name, enemy.hp))
			print("")
			
	def heal(self):
		consumables = [item for item in self.inventory if isinstance(item, items.Consumable)]
		
		if not consumables:
			print("You don't have any items to heal you!")
			return
			
		for i, item in enumerate(consumables, 1):
			print("Choose an item to heal yourself: ")
			print("{}. {}".format(i, item))
			
		valid = False
		while not valid:
			choice = input("Select one: ")
			try:
				to_eat = consumables[int(choice) - 1]
				self.hp = min(100, self.hp + to_eat.healing_value)
				self.inventory.remove(to_eat)
				print("current HP: {}".format(self.hp))
				valid = True
			except(ValueError, IndexError):
				print("Invalid choice, try again.")