#Adventure Tutorial by Eric Wideman 07/25/2016

import random
import enemies

class MapTile:
	def __init__(self, x, y):
		self.x = x
		self.y = y
		
	def intro_text(self):
		raise NotImplementedError("Create a subclass instead!")
		
	def modify_player(self, player):
		pass
		
class StartTile(MapTile):
	def intro_text(self):
		return """
			You wake up, sore and confused. As you look around you can make out
			the shape of chairs and a table. There is a flickering candle that is
			your only source of light."""
			
class HallwayTile(MapTile):
	def intro_text(self):
		return """
			You found a hallway, it appears to be empty but it never hurts
			to keep an eye out. It is rather dark in here..."""
			
class  VictoryTile(MapTile):
	def intro_text(self):
		return """
			This appears to be the exit. The room is empty and you take a deep breath.
			You've done it, you found the way out..."""
			
class EnemyTile(MapTile):
	def __init__(self, x, y):
		r = random.random()
		if r > 0.50:
			self.enemy = enemies.GiantSpider()
			self.alive_text = "A giant spider climbs down from " \
							"it's web landing right in front of you!"
			self.dead_text = "The corpse of a dead spider spasms a " \
							"few times and then stiffens up."
		elif r > 0.80:
			self.enemy = enemies.Ogre()
			self.alive_text = "An ogre is blocking your path!"
			self.dead_text = "A dead ogre lays at your feet, " \
							"it's blood begins to pool."
		elif r > 0.95:
			self.enemy = enemies.BatColony()
			self.alive_text = "You hear a squeaking noise gwoing louder " \
							"...suddenly you are lost in a swarm of bats!"
			self.dead_text = "Dozens of dead bats are scattered on the ground."
			
		elif r > 30:
			self.enemy = enemies.Kobold()
			self.alive_text = "Standing in the corner of the room you see a nasty " \
							"little creature hissing and spitting at you."
			self.dead_text = "The kobold gasps one last, desperate breath " \
							"before collapsing and spasming on the floor."
		
		else:
			self.enemy = enemies.RockMonster()
			self.alive_text = "You've disturbed a rock monster " \
							"from his slumber!"
			self.dead_text = "Defeated, the monster has reverted " \
							"into an ordinary rock."
			
		super().__init__(x, y)
	
	def intro_text(self):
		text = self.alive_text if self.enemy.is_alive() else self.dead_text
		return text
			
	def modify_player(self, player):
		if self.enemy.is_alive():
			player.hp = player.hp - self.enemy.damage
			print("Enemy does {} damage. You have {} HP remaining.".format(self.enemy.damage, player.hp))
			print("")

world_dsl = """
|  |VT|  |
|  |EN|  |
|EN|ST|EN|
|  |EN|  |
"""

def is_dsl_valid(dsl):
	if dsl.count("|ST|") != 1:
		return False
	if dsl.count("|VT|") == 0:
		return False
	lines = dsl.splitlines()
	lines = [l for l in lines if l]
	pipe_counts = [line.count("|") for line in lines]
	for count in pipe_counts:
		if count != pipe_counts[0]:
			return False
			
	return True
			
#World Map
world_map = [
	[VictoryTile(0,0), HallwayTile(1,0), EnemyTile(2,0)],
	[EnemyTile(0,1), EnemyTile(1,1), HallwayTile(2,1)],
	[EnemyTile(0,2), HallwayTile(1,2), EnemyTile(2,2)],
	[HallwayTile(0,3), EnemyTile(1,3), StartTile(2,3)]
]

#Tile Coordinate Checker
def tile_at(x, y):
	if x < 0 or y < 0:
		return None
	try:
		return world_map[y][x]
	except IndexError:
		return None